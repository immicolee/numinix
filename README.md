Assume and modification:
1. for "add to cart" and "instant purchase" i used 210 px for all devices instead of 290px;
2. for line-height and font-size property, mostly i set it according to the css that i extract from photoshop, not sure if that is what the designer what
3. for the tea and stuff menu, since the designer put all the text in one text box, so i did not set any margin nor padding.
4. i do not change the default size for container class, so it kind of affect the design a bit. one thing that i would like to mention is the slider. According to the design the height is 680px and width is 888px , however, the width can not reach 888px. so the image size shrink a big. so i made it vertical align center instead of fullfill the div
5. for the "man tea" logo, instead using grid system and put it in the 3rd column, i used margin-left property, i am not sure if that is what the designer want. 
6. for the navigation bar, i change the text color for all hover effect. for the underscore effect on categories tab, i assume it is a dropdown menu show effect, so i only use it for categories tab which has a dropdown menu
7. for description and reviews tab, i did not see any hover effect, so i assume it will change text color for hover.